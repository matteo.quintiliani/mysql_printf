# mysql\_printf

`mysql_printf` is a mysql client command line tool for formatting query results by *printf-like* string format.

Former development of `mysql_printf` was called `mysql_format`. You can run it using indifferently both names.

The *printf-like* strings can contain conversion specifiers `%s` to customize the indentation and position of the column values in the query results.

You can arrange the values of a single query row as a list of Delimiter-Separated-Values (i.e. CSV, _comma separated values_), or formatting them in a more complex way, for instance arranging the values of a single query row result in several customized lines.

`mysql_printf` can be very useful to generate custom text files from database information.

## Some other information

Without defining a *printf-like* string format, the default text output format is a list of delimiter-separated-values where delimiter is `';'`, but you can change with whatever character you want.

`mysql_printf` handles all attribute values as strings, in fact it can accept only the printf conversion specifiers `%s`. You can rely on powerful MySQL functions for pre-processing output fields format, mathematic computation or string concatenations.

Examples of possible conversion specifiers:

 * `%s` (_right aligned_)
 * `%-s` (_left aligned_)
 * `%10s` (_width 10 right aligned_)
 * `%-3s` (_width 3 left aligned_)

The conversion specifiers `%s` within the *printf-like* strings have to be as many as the fields of the query result, except when using the option `-m` (see later).

`mysql_printf` is also able to process the escape sequence, such as `\n`, `\t`, etc.

Let's see some examples.

### Very simple example

Formatting query result to CSV (comma separated values). Use option `-e` followed by the separator character value.

    mysql_printf -u myuser -p mypass -d mydbname \
    -S "SELECT ... FROM ..." \
    -e ','


### Simple example

Suppose you have the following MySQL table `People`:

```sql
    CREATE TABLE People (
	    FirstName VARCHAR(255),
	    LastName  VARCHAR(255),
	    Age       BIGINT,
	    City      VARCHAR(255)
    );

    INSERT INTO People VALUES
    ('Mary', 'Line', 47, 'New York'),
    ('Roberto', 'Bianchi', 35, 'Milano'),
    ('Jean-Marie', 'Ogret', 41, 'Paris');
```

and you want to format the output like this:

    Name: Mary Line             Age: 47   City: New York                 
    Name: Roberto Bianchi       Age: 35   City: Milano                   
    Name: Jean-Marie Ogret      Age: 41   City: Paris

Then, you can run `mysql_printf` with the following query:

    SELECT Concat(FirstName, ' ', LastName), Age, City FROM People

and the following *printf-like* string format:

    "Name: %-20s  Age: %2s   City: %-25s\n"

The complete command line is	:

    mysql_printf -u myuser -p mypass -d mydbname \
    -S "SELECT Concat(FirstName, ' ', LastName), Age, City FROM People" \
    -F "Name: %-20s  Age: %2s   City: %-25s"

### Intermediate example

Suppose you want to generate lists of people based on the city they live, a sort of *group by* city but saving them in different files, one for each different city. You can build the filename by the first field of your query result and run `mysql_printf` with the option `-m`:

    mysql_printf -u myuser -p mypass -d mydbname \
    -S "SELECT Concat(Replace(City, ' ', '_'), '.txt'), Concat(FirstName, ' ', LastName), Age FROM People" \
    -F "Name: %-20s  Age: %2s\n" \
    -m

Note: the number of conversion specifiers `%s` is one less than the number of fields of the query. The first field result is used for building the filename.

### Advanced example

**TODO**: Writing output from in several files from multiple queries. Options `-m` and `-a`.


## Dependencies and source compilation

### libmysqlclient is required

`mysql_printf` relies on library called `libmysqlclient`. Usually this library is released with the utility `mysql_config` which semplifies much the compilation phase.

If you do not have already installed MySQL you can get it from <http://dev.mysql.com/downloads/mysql/>

### pgk-config and libconfig are optionals

If you want to avoid to explicit everytime the database parameters connection you can save everything in a file a recall parameters automatically. To do this, you need to install the optional library called `libconfig` <http://www.hyperrealm.com/libconfig>. To install it you need also `pkg-config` <http://pkg-config.freedesktop.org/>.

### Compilation

#### Quick

```
./configure
make
make install
```

#### Other examples

```
./configure CC=cc PKG_CONFIG_PATH=/usr/local/lib/pkgconfig
./configure --with-mysql-config=mysql_config5
```

## Contributing

Thank you for considering contributing to this development.

## Author

Matteo Quintiliani - Istituto Nazionale di Geofisica e Vulcanologia - Italy

Mail bug reports and suggestions to *matteo.quintiliani [at] ingv.it*

## License

Software is open-source and released under GNU Library General Public License.

